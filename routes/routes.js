const phoneRoutes = require('./phones');
const appRouter = (app, fs) => {

    app.get('/', (req, res) => {
        res.send('this is my api');
    });

    phoneRoutes(app, fs);
};

module.exports = appRouter;