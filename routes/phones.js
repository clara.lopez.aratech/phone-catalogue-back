const phoneRoutes = (app, fs) => {

    // variables
    const dataPath = './data/phones.json';

    // READ
    app.get('/phones', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }

            res.send(JSON.parse(data));
        });
    });

    app.get('/phones/:id', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            const phoneId = parseInt(req.params["id"]);
            const phoneSelected = JSON.parse(data).find(p => p.id === phoneId);
            if (phoneSelected) {
                res.send(phoneSelected);
            } else {
                res.send('The id does not exist');
            }
        });
    });
};

module.exports = phoneRoutes;