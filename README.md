## Phone Catalogue

A Node.js app using Express(v4.17.1).

## Running Locally

Make sure you have Node.js installed.

Clone down this repository.

Installation:

### `npm install`

To Start Server:

### `npm start`

Your app should now be running on http://localhost:3001/